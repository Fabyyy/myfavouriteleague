import React, {Component} from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class StartingPageContent extends Component {
    render() {
        return <div>This is some introductional content!</div>;
    }
}

export default StartingPageContent;
